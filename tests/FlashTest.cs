using System;
using System.Collections.Generic;
using Xunit;
using FluentAssertions;
using HumbleProgrammer;
using HumbleProgrammer.Characters;
using Xunit.Abstractions;

namespace Tests
{
    public class FlashTest
    {
        [Theory]
        [InlineData(5), InlineData(0), InlineData(10)]
        public void FlashImplementations(int wins)
        {
            var barryAllen = new Flash(wins,"Flash");
            
            barryAllen.Cooldown.Should().BePositive();
            barryAllen.Damage.Should().BePositive();
            barryAllen.Hp.Should().BePositive();
            barryAllen.Speed.Should().BePositive();
        }

        [Theory]
        [InlineData(5, 75), InlineData(0, 50), InlineData(10, 100)]
        public void FlashDamage(int wins, int dmg)
        {
            var barryAllen = new Flash(wins,"Flash");
            
            barryAllen.Damage.Should().Be(dmg);
        }
        
        [Theory]
        [InlineData(5, 51), InlineData(0, 1), InlineData(10, 101)]
        public void FlashSpeed(int wins, int speed)
        {
            var barryAllen = new Flash(wins,"Flash");
            
            barryAllen.Speed.Should().Be(speed);
        }
        
        [Fact]
        public void FlashSpecial()
        {
            var barryAllen = new Flash(0,"Flash");
            
            barryAllen.SpecialAction(new List<Character>() {barryAllen}, new List<Character>());
            barryAllen.Hp.Should().Be(110);
        }
    }
}