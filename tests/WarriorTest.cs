using System.Collections.Generic;
using Xunit;
using FluentAssertions;
using HumbleProgrammer.Characters;

namespace Tests
{
    public class WarriorTest
    {


        [Theory]
        [InlineData(1, 15), InlineData(0, 5), InlineData(10, 105)]
        public void WarriorDamageAssertions(int wins, int expected)
        {
           var w1 = new Warrior(wins);
           w1.Damage.Should().Be(expected);
        }

        [Theory]
        [InlineData(1, 6), InlineData(0, 1), InlineData(10, 51)]
        public void WarriorSpeedAssertions(int wins, int expected)
        {
            var w1 = new Warrior(wins);
            w1.Speed.Should().Be(expected);
        }

        [Theory]
        [InlineData(1), InlineData(0), InlineData(10)]
        public void WarriorNameAssertions(int wins)
        {
            var w1 = new Warrior(wins);
            w1.Name.Should().Be("W1");
        }

        [Theory]
        [InlineData(1), InlineData(0), InlineData(10)]
        public void WarriorHealthAssertions(int wins)
        {
            var w1 = new Warrior(wins);
            w1.Hp.Should().Be(100);
        }

        [Theory]
        [InlineData(100, 20), InlineData(200, 40), InlineData(0, 0)]
        public void WarriorSpecialActionAssertions(int hp, int expected)
        {
            var w1 = new Warrior(1);

            var allies = new List<Character>();
            var enemies = new List<Character>();
            allies.Add(w1);
            enemies.Add(new MockCharacter(hp, 30));

            w1.SpecialAction(allies, enemies)[0].Should().Be($"The warrior has done 80% damage to {enemies[0].Name}!");
            enemies[0].Hp.Should().Be(expected);

        }

    }
}