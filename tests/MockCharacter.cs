using System;
using System.Collections.Generic;
using Xunit;
using FluentAssertions;
using HumbleProgrammer.Characters;

namespace Tests
{
    class MockCharacter : Character
    {
        public MockCharacter()
        {
        }
        
        public MockCharacter(int hp, int damage)
        {
            this.Hp = hp;
            this.Damage = damage;
        }
        
        public string Name { get; }
        public int Cooldown { get; } = 2;
        public int Damage { get; }
        public int Hp { get; set; } 
        public int Speed { get; }
        public List<string> SpecialAction(List<Character> allies, List<Character> enemies)
        {
            // throw new System.NotImplementedException();
            // throw new System.NotImplementedException();
            enemies[0].Hp /= 5;
            var result = new List<String>();
            result.Add($"The warrior has done 80% damage to {enemies[0].Name}!");
            return result;
        }
    }
}