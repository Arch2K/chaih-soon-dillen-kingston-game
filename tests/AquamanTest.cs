using System.Collections.Generic;
using Xunit;
using FluentAssertions;
using HumbleProgrammer.Characters;
namespace Tests
{
    public class AquamanTest
    {
        [Theory]
        [InlineData(5,25),InlineData(3,15),InlineData(8,40)]
        public void CharacterDamageTest(int wins, int expectDmg)
        {
            var c1 = new Aquaman(wins);
            c1.Damage.Should().Be(expectDmg);
        }
        
        [Theory]
        [InlineData(2,4),InlineData(10,20),InlineData(16,32)]
        public void CharacterSpeedTesT(int wins, int expectSp)
        {
            var c1 = new Aquaman(wins);
            c1.Speed.Should().Be(expectSp);
        }
    }
}