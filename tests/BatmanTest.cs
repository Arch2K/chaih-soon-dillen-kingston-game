using System.Collections.Generic;
using Xunit;
using FluentAssertions;
using HumbleProgrammer.Characters;
namespace Tests
{
    public class BatmanTest
    {
        [Theory]
        [InlineData(5,25),InlineData(3,15),InlineData(8,40)]
        public void CharacterDamageTest(int wins, int expectDmg)
        {
            var c1 = new Batman(wins);
            c1.Damage.Should().Be(expectDmg);
        }
        
        [Theory]
        [InlineData(2,4),InlineData(10,20),InlineData(16,32)]
        public void CharacterSpeedTesT(int wins, int expectSp)
        {
            var c1 = new Batman(wins);
            c1.Speed.Should().Be(expectSp);
        }

        [Fact]
        public void CharacterSpeicalSkill()
        {
            var c1 = new Batman(1);
            var c2 = new Batman(2);
            var c3 = new Batman(2);
            List<Character> charList1 = new List<Character>();
            List<Character> charList2 = new List<Character>();
            charList1.Add(c1);
            charList2.Add(c2);
            charList2.Add(c3);

            c1.SpecialAction(charList1, charList2).Should()
                .BeEquivalentTo(new List<string>() {c1.Name + " wiped out the whole team."});
        }
    }
}