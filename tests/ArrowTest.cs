using System;
using System.Collections.Generic;
using Xunit;
using FluentAssertions;
using HumbleProgrammer;
using HumbleProgrammer.Characters;
using Xunit.Abstractions;

namespace Tests
{
    public class ArrowTest
    {
        [Theory]
        [InlineData(5), InlineData(0), InlineData(10)]
        public void ArrowImplementations(int wins)
        {
            var oliver = new Arrow(wins,"Oliver");
            
            oliver.Cooldown.Should().BePositive();
            oliver.Damage.Should().BePositive();
            oliver.Hp.Should().BePositive();
            oliver.Speed.Should().BePositive();
        }

        [Theory]
        [InlineData(5, 15), InlineData(0, 10), InlineData(10, 20)]
        public void ArrowDamage(int wins, int dmg)
        {
            var oliver = new Arrow(wins,"Oliver");
            
            oliver.Damage.Should().Be(dmg);
        }
        
        [Theory]
        [InlineData(5, 10), InlineData(0, 10), InlineData(10, 10)]
        public void ArrowSpeed(int wins, int speed)
        {
            var oliver = new Arrow(wins,"Oliver");
            
            oliver.Speed.Should().Be(speed);
        }
    }
}