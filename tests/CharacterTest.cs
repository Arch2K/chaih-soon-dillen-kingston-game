using System.Security.Cryptography.X509Certificates;
using Xunit;
using FluentAssertions;
using HumbleProgrammer.Characters;

namespace Tests
{
    public class CharacterTest
    {
        [Fact]
        public void CharacterValidation()
        {
            var c1 = new Batman(5);
            c1.Hp.Should().BePositive().And.NotBe(0);
            c1.Name.Should().NotBeEmpty();
            c1.Cooldown.Should().BePositive().And.NotBe(0);
            c1.Speed.Should().BePositive().And.NotBe(0);
        }

        [Theory]
        [InlineData(1), InlineData(0), InlineData(10)]
        public void WarriorDamageAssertions(int wins)
        {
            var w1 = new Warrior(wins);
            w1.Damage.Should().BePositive();
        }

        [Theory]
        [InlineData(1), InlineData(0), InlineData(10)]
        public void WarriorCooldownAssertions(int wins)
        {
            var w1 = new Warrior(wins);
            w1.Cooldown.Should().BePositive();
            w1.Cooldown.Should().Be(2);
        }

        [Theory]
        [InlineData(1), InlineData(0), InlineData(10)]
        public void WarriorSpeedAssertions(int wins)
        {
            var w1 = new Warrior(wins);
            w1.Speed.Should().BePositive();
        }

        [Theory]
        [InlineData(1), InlineData(0), InlineData(10)]
        public void WarriorNameAssertions(int wins)
        {
            var w1 = new Warrior(wins);
            w1.Name.Should().NotBe("");
        }

        [Theory]
        [InlineData(1), InlineData(0), InlineData(10)]
        public void WarriorHealthAssertions(int wins)
        {
            var w1 = new Warrior(wins);
            w1.Hp.Should().BePositive();
        }
    }
}