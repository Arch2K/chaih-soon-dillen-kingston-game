using System;
using System.Collections.Generic;

namespace HumbleProgrammer.Characters
{
    public class Batman : Character
    {
        private int _wins;
        public string Name { get; } = "Aquaman";

        public int Cooldown { get; } = 10;
        public int Damage => _wins * 5;
        
        public int Hp { get; set; } = 100; 
        public int Speed => 2 * _wins;

        public List<string> SpecialAction(List<Character> allies, List<Character> enemies)
        {
            foreach (Character c in enemies)
            {
                c.Hp = 0;
            }

            return new List<string>{this.Name + " wiped out the whole team."};
        }

        public Batman(int win)
        {
            this._wins = win;
        }
    }
}