using System.Collections.Generic;

namespace HumbleProgrammer.Characters
{
    public class GrimStroke: Character
    {
        private int _wins;
        public string Name { get; } = "Grim Stroke";
        public int Cooldown { get; } = 5;

        public int Damage => 5 * _wins + 2;

        public int Hp { get; set; } = 50;
        public int Speed => 5 * _wins + 1;

        public List<string> SpecialAction(List<Character> allies, List<Character> enemies)
        {
            if (enemies.Count <= 0) return new List<string>();
            var selfdmg = Hp / 2;
            Hp -= selfdmg;
            enemies[0].Hp -= selfdmg * 3 / 2;
            return new List<string>{$"{Name} used Blood Pact, sacrificing {selfdmg} HP to deal {selfdmg * 3 / 2} damage to {enemies[0].Name}"};

        }

        public GrimStroke(int wins)
        {
            this._wins = wins;
        }
    }
}