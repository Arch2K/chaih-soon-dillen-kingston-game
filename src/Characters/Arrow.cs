using System.Collections.Generic;

namespace HumbleProgrammer.Characters
{
    public class Arrow : Character
    {
        private readonly int _wins;
        public string Name { get; }
        public int Cooldown { get; } = 5;
        public int Damage => _wins + 10;
        public int Hp { get; set; } = 100;
        public int Speed { get; } = 10;
        public List<string> SpecialAction(List<Character> allies, List<Character> enemies)
        {
            var log = new List<string>();
            
            foreach (var enemy in enemies)
            {
                enemy.Hp -= 10;
                log.Add(this.Name + " spread shot " + enemy.Name + " to " + enemy.Hp);
            }
            
            return log;
        }
        
        public Arrow(int wins, string name)
        {
            _wins = wins;
            Name = name;
        }
    }
}