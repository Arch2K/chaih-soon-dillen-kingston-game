using System;
using System.Collections.Generic;
using System.ComponentModel;
using Microsoft.VisualBasic;

namespace HumbleProgrammer.Characters
{
    public class Warrior : Character
    {
        private int _wins;

        public Warrior(int wins)
        {
            this._wins = wins;
        }

        public string Name { get; } = "W1";
        public int Cooldown { get; } = 2;

        public int Damage
        {
            get
            {
                return (this._wins * 10) + 5;
            }
        }

        public int Hp { get; set; } = 100;

        public int Speed
        {
            get { return (this._wins * 5) + 1; }
        }

        public List<string> SpecialAction(List<Character> allies, List<Character> enemies)
        {
            // throw new System.NotImplementedException();
            enemies[0].Hp /= 5;
            var result = new List<String>();
            result.Add($"The warrior has done 80% damage to {enemies[0].Name}!");
            return result;
        }
    }
}
