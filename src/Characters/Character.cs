using System.Collections.Generic;

namespace HumbleProgrammer.Characters
{
    public interface Character
    {
        string Name { get; }
        int Cooldown { get; }
        int Damage { get; }
        int Hp { get; set; }
        int Speed { get; }
        List<string> SpecialAction(List<Character> allies, List<Character> enemies);
    }
}