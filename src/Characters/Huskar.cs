using System.Collections.Generic;

namespace HumbleProgrammer.Characters
{
    public class Huskar: Character
    {
        private int _wins;
        private int _rageStacks = 0;
        public string Name { get; } = "Huskar";
        public int Cooldown { get; } = 3;

        public int Damage => 2 * _wins + 2 + _rageStacks;

        public int Hp { get; set; } = 50;
        public int Speed => 5 * _wins + 10;

        public List<string> SpecialAction(List<Character> allies, List<Character> enemies)
        {
            if (enemies.Count <= 0) return new List<string>();
            var addStacks = 2 * ((50 - Hp) / 5 + 1);
            _rageStacks += addStacks;
            var logs = new List<string> {$"{Name} is in a rage! {Name} gains {addStacks} rage!"};
            logs.Add($"{Name} attacks all enemies in a rage!");
            foreach(var enemy in enemies)
            {
                enemies[0].Hp -= Damage / 3;
                logs.Add($"{Name} dealt {Damage / 3} to {enemy.Name}!");
            }
            
            return logs;

        }

        public Huskar(int wins)
        {
            this._wins = wins;
        }
    }
}