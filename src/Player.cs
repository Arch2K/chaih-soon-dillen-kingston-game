using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using HumbleProgrammer.Characters;

namespace HumbleProgrammer
{
    [ExcludeFromCodeCoverage]
    public class Player
    {
        public Guid Id { get; }
        public List<Character> Characters { get; }

        public Player(Guid id, List<Character> characters)
        {
            Id = id;
            Characters = characters;
        }
    }
}