using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using HumbleProgrammer.Characters;

namespace HumbleProgrammer
{
    [ExcludeFromCodeCoverage]
    public class Game
    {
        private readonly Player _player1;
        private readonly Player _player2;

        public Game(Player player1, Player player2)
        {
            _player1 = player1;
            _player2 = player2;
        }

        public enum Winner
        {
            Player1,
            Player2,
            None,
        };

        // Check if there is a winner, or not
        public Winner CheckWinner(List<Character> player1Units, List<Character> player2Units)
        {
            // Default to player1 winner if both die
            if (player2Units.Where(x => x.Hp > 0).Count() == 0) return Winner.Player1;
            if (player1Units.Where(x => x.Hp > 0).Count() == 0) return Winner.Player2;
            return Winner.None;
        }

        // Should run through a whole battle, return the logs of the battle and return the winning player!
        public (List<string>, Player) SimulateMatch()
        {
            var p1Speed = _player1.Characters.Sum(x => x.Speed);
            var p2Speed = _player2.Characters.Sum(x => x.Speed);
            var logs = new List<string>();
            var result = CheckWinner(_player1.Characters, _player2.Characters);
            var step = 0;
            while (result == Winner.None)
            {
                if (p2Speed > p1Speed)
                {
                    logs.AddRange(Step(step, _player2.Characters, _player1.Characters));
                }
                else
                {
                    logs.AddRange(Step(step, _player1.Characters, _player2.Characters));
                }
                result = CheckWinner(_player1.Characters, _player2.Characters);
                step++;
            }

            return result switch
            {
                Winner.Player1 => (logs.Append("Player 1 wins!").ToList(), _player1),
                Winner.Player2 => (logs.Append("Player 2 wins!").ToList(), _player2),
                _ => throw new InvalidDataException()
            };
        }

        // Based on the number of steps ran, it should decide to cast skill or do normal attack, and return the logs
        // for number of steps taken
        public List<string> Step(int step, List<Character> FirstUnits, List<Character> SecondUnits)
        {
            var logs = new List<string> {$"Turn {step}:"};
            var fQueue = new Queue<Character>(FirstUnits);
            var sQueue = new Queue<Character>(SecondUnits);
            while (fQueue.Count > 0 || sQueue.Count > 0)
            {
                var first = NextAlive(fQueue);
                if (first != null)
                {
                    var target = NextTarget(SecondUnits);
                    if (target == null) break;
                    logs.AddRange(Act(first, step, target, AliveCharacters(FirstUnits),
                        AliveCharacters(SecondUnits)));
                }

                var second = NextAlive(sQueue);
                if (second != null)
                {
                    var target = NextTarget(FirstUnits);
                    if (target == null) break;
                    logs.AddRange(Act(second, step, target, AliveCharacters(SecondUnits),
                        AliveCharacters(FirstUnits)));
                }
            }

            return logs;
        }

        public Character NextAlive(Queue<Character> queue)
        {
            if (queue.Count == 0) return null;
            var c = queue.Dequeue();
            while (queue.Count > 0 && c.Hp <= 0)
            {
                c = queue.Dequeue();
            }

            return c.Hp <= 0 ? null : c;
        }

        public Character NextTarget(List<Character> ls)
        {
            return ls.FirstOrDefault(x => x.Hp > 0);
        }

        public List<Character> AliveCharacters(List<Character> ls)
        {
            return ls.Where(x => x.Hp > 0).ToList();
        }

        public List<string> Act(Character actor, int step, Character target, List<Character> allies,
            List<Character> enemies)
        {
            var dead = enemies.Concat(allies)
                .Where(x => x.Hp <= 0);
            var logs = new List<string>();
            if (step % actor.Cooldown == 0)
            {
                logs.AddRange(actor.SpecialAction(allies, enemies));
            }
            else
            {
                target.Hp -= actor.Damage;
                logs.Add($"{actor.Name} dealt {actor.Damage} to {target.Name}!");
            }

            var newDead = enemies.Concat(allies)
                .Where(x => x.Hp <= 0);
            var DeathLog = newDead.Except(dead)
                .Select(x => $"{x.Name} died!");
            return logs.Concat(DeathLog).ToList();
        }
    }
}