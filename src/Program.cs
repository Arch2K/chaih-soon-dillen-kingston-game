﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using HumbleProgrammer.Characters;

namespace HumbleProgrammer
{
    [ExcludeFromCodeCoverage]
    class Program
    {
        public static void Main(string[] args)
        {
            var p1 = new Player(Guid.NewGuid(), new List<Character>{new Huskar(1), new Warrior(1)});
            var p2 = new Player(Guid.NewGuid(), new List<Character>{new GrimStroke(1), new GrimStroke(1)});
            var game = new Game(p1, p2);
            var (logs, winner) = game.SimulateMatch();
            Console.WriteLine(logs.Aggregate((x,y) => x + "\n" + y));
        }
    }
}